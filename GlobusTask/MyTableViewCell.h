//
//  MyTableViewCell.h
//  GlobusTask
//
//  Created by Kriss Violense on 08.05.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *logoLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskLabel;
@property (weak, nonatomic) IBOutlet UILabel *discriptionTaskLabel;


@end
