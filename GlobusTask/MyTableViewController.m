//
//  MyTableViewController.m
//  GlobusTask
//
//  Created by Kriss Violense on 08.05.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "MyTableViewController.h"
#import "MyTableViewCell.h"
#import "MyViewController.h"
#import "Task.h"

@implementation MyTableViewController

- (void)viewDidLoad {
    tasksCount = 0;
    [super viewDidLoad];
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit  target:self action:@selector(actionEdit:)];
    UIBarButtonItem* addButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(actionAdd:)];

    self.navigationItem.leftBarButtonItem = editButton;
    self.navigationItem.rightBarButtonItem = addButton;
    
    taskArray = [[NSMutableArray alloc] init];
    for(int i = 0; i < 5; ++i) {
        Task *newTask = [[Task alloc] init];
        newTask.title = [NSString stringWithFormat:@"Поручение № %d", i+1];
        newTask.discription = [NSString stringWithFormat:@"Описание поручения № %d", i+1];
        [taskArray addObject: newTask];
        tasksCount ++;

    }
    self.title =@"GLOBUS";
    
}

-(void)actionAdd:(UIBarButtonItem *) sender {
    tasksCount ++;
    Task *newTask = [[Task alloc] init];
    newTask.title = [NSString stringWithFormat:@"Поручение № %d", tasksCount];
    newTask.discription = [NSString stringWithFormat:@"Описание поручения № %d", tasksCount];
    [taskArray addObject: newTask];
    [self.tableView reloadData];

}

-(void)actionEdit:(UIBarButtonItem *) sender {
 
    BOOL isEditing = self.tableView.editing;
    
    [self.tableView setEditing:!isEditing animated:YES];
    UIBarButtonSystemItem item = UIBarButtonSystemItemEdit;
    if (self.tableView.editing) {
        item = UIBarButtonSystemItemDone;
    }
    
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:item  target:self action:@selector(actionEdit:)];
    [self.navigationItem setLeftBarButtonItem:editButton animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return taskArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Task *currentTask = taskArray[indexPath.row];
    cell.taskLabel.text = currentTask.title;
    cell.discriptionTaskLabel.text = currentTask.discription;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Task * taskForRemove = taskArray[indexPath.row];
        [taskArray removeObject:taskForRemove];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView endUpdates];
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"myView"];
    myView.task = taskArray[indexPath.row];
    [self.navigationController pushViewController:myView animated:YES];
}

@end
