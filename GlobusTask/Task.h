//
//  Task.h
//  GlobusTask
//
//  Created by Kriss Violense on 09.05.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

@property(strong) NSString *title;
@property(strong) NSString *discription;

@end
