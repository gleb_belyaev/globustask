//
//  MyTableViewController.h
//  GlobusTask
//
//  Created by Kriss Violense on 08.05.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"

@interface MyTableViewController : UITableViewController {
    NSMutableArray *taskArray;
    int tasksCount;
}
@property (assign,nonatomic) Task *currentTask;

@end
