
//  MyViewController.m
//  GlobusTask
//
//  Created by Kriss Violense on 08.05.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import "MyViewController.h"

@implementation MyViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.taskLabel setText: self.task.title];
    CGRect labelFrame = CGRectMake(40, 167, 312, 150);
    UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
    [myLabel setText:self.task.discription];
    
    // Tell the label to use an unlimited number of lines
    [myLabel setNumberOfLines:0];
    [myLabel sizeToFit];
    
    [self.view addSubview:myLabel];
}

@end
