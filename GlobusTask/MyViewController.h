//
//  MyViewController.h
//  GlobusTask
//
//  Created by Kriss Violense on 08.05.16.
//  Copyright © 2016 Kriss_Violense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"

@interface MyViewController : UIViewController

@property (assign,nonatomic) Task *task;
@property (weak, nonatomic) IBOutlet UILabel *taskLabel;

@end
